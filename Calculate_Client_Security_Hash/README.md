**Calculate Client Security Hash**

In this Process Bot Logs in ACME web site https://acme-test.uipath.com .

Store the login id and password as a credential on UiPath Orchestrator.

Navigate to the Workitems.

Use Data Scraping to Scrap only WI5 items with open status from WorkItems.

Scrap ClientName,ClientCity and ClientID from Client Information Details.

Went to SHA1 Online and paste scraped ClientName,ClientCity and ClientID.

Search for the HashCode.

Scrap HashCode from SHA1.

Update the all WI5 WorkItem with HashCode.

The whole process is done by robot. No need to login or logout manually.I have used UiPath Studio Activites,UiPathOrchestrator to automate this whole process.




---
